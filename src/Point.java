/*
 * Created by Le Cong Thanh in 5/10/2018
 *
 */
public class Point {
    String subjectId;
    String subjectName;
    String studentId;
    String studentName;
    double point;
    int credit;
    public Point(String subjectId, String subjectName, int credit, String studentId,String studentName){
        this.subjectId = subjectId;
        this.studentId = studentId;
        this.studentName = studentName;
        this.subjectName = subjectName;
        this.credit = credit;
        point = -1.0;
    }
    public Point(String subjectId, String subjectName, int credit, String studentId,
                 String studentName,double point){
        this(subjectId,subjectName,credit,studentId,studentName);

        this.point = point;

    }



    public void printInfomation(){
        System.out.println(this.getStudentId() + " : "+ this.getStudentName()
                + " : " + this.getSubjectId() + " : "+this.getSubjectName() + " : "+this.getPoint());
    }

    public String getSubjectId() {
        return subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public String getStudentId() {
        return studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public double getPoint() {
        return point;
    }

    public int getCredit() {
        return credit;
    }

    public void setPoint(double point) {
        this.point = point;
    }
}
