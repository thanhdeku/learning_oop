import java.util.Date;

public class HustPerson extends HustObject {
    protected Date birthday;
    protected String major;

    public Date getBirthday() {
        return birthday;
    }

    public String getMajor() {
        return major;
    }
}
