import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Database {
    List<Student> students ;
    List<Lecturer> lecturers;
    List<Subject> subjects;
    List<Course> courses;
    PointList pointList ;
    public Database(){
        pointList = new PointList();
        students = new ArrayList<>();
        students.add(new Student("Le Cong Thanh", "20164834","02/05/1998",
                "Infomatic Technology","Talented Program" ));
        students.add(new Student("Nguyen Tien Tai", "20164837","01/01/1998",
                "Infomatic Technology","Talented Program" ));
        students.add(new Student("Le Kha Hai", "20164836","01/05/1998",
                "Infomatic Technology","Talented Program" ));
        students.add(new Student("Le Ba Truong Giang", "20164776","01/05/1998",
                "Infomatic Technology","Talented Program" ));
        lecturers = new ArrayList<>();
        lecturers.add(new Lecturer("Huynh Thi Thanh Binh","IT101"));
        lecturers.add(new Lecturer("Nguyen Thanh Hung","IT102"));
        lecturers.add(new Lecturer("Ngo Hong Son","IT103"));
        lecturers.add(new Lecturer("Huynh Quyet Thang","IT104"));
        subjects = new ArrayList<>();
        subjects.add(new Subject("IT101","Computer Network","SOICT",3));
        subjects.add(new Subject("IT102","Java-OOP","SOICT",3));
        subjects.add(new Subject("IT103","Evolutionary Computing","SOICT",3));
        subjects.add(new Subject("IT101","Linux & Open Software","SOICT",3));
        courses = new ArrayList<>();
        courses.add(new Course("IT3001",subjects.get(0),lecturers.get(0)));
        courses.add(new Course("IT3002",subjects.get(1),lecturers.get(1)));
        courses.add(new Course("IT3003",subjects.get(2),lecturers.get(2)));
        courses.add(new Course("IT3004",subjects.get(3),lecturers.get(3)));
    }

    public Student getStudents(String id) {
        Student student = null;
        for (int i = 0; i < students.size(); i++) {
            if(Objects.equals(students.get(i).getId(), id)){
                student = students.get(i);
            }
        }
        if(student == null){
            System.out.println("invalid id");
            return null;
        }else {
            return student;
        }
    }

    public Lecturer getLecturer(String id) {
        Lecturer lecturer = null;
        for (int i = 0; i < lecturers.size(); i++) {
            if(Objects.equals(lecturers.get(i).getId(), id)){
                lecturer = lecturers.get(i);
            }
        }
        if(lecturer == null){
            System.out.println("invalid id");
            return null;
        }else {
            return lecturer;
        }
    }

    public Subject getSubjects(String id) {
        Subject subject = null;
        for (int i = 0; i < subjects.size(); i++) {
            if(Objects.equals(students.get(i).getId(), id)){
                subject = subjects.get(i);
            }
        }
        if(subject == null){
            System.out.println("invalid id");
            return null;
        }else {
            return subject;
        }
    }

    public Course getCourses(String id) {
        Course course = null;
        for (int i = 0; i < courses.size(); i++) {
            if(Objects.equals(courses.get(i).getId(), id)){
                course = courses.get(i);
            }
        }
        if(course == null){
            System.out.println("invalid id");
            return null;
        }else {
            return course;
        }
    }

    public void add(Student student){
        students.add(student);
    }
    public void add(Subject subject){
        subjects.add(subject);
    }
    public void add(Lecturer lecturer){
        lecturers.add(lecturer);
    }
    public void add(Course course){
        courses.add(course);
    }

    public List<Student> getStudents() {
        return students;
    }

    public List<Lecturer> getLecturers() {
        return lecturers;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public PointList getPointList() {
        return pointList;
    }
}
