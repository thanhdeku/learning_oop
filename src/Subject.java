/*
 * Created by Le Cong Thanh in 5/10/2018
 *
 */


public class Subject extends HustObject {
    int credit;
    public Subject(String id, String name, String department, int credit){
        this.id = id;
        this.name = name;
        this.department = department;
        this.credit = credit;
    }

    public int getCredit() {
        return credit;
    }

    public void printInformation(){
        System.out.println("_____________________________________________________________");
        System.out.println(" ");
        System.out.println("        Name         : " + this.getName());
        System.out.println("        Id           : " + this.getId());
        System.out.println("        Department   : " + this.getDepartment());
        System.out.println("_____________________________________________________________");

    }
}
