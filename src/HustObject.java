public class HustObject {
    protected String id;
    protected String name;
    protected String department;
    /**
     *@Getter/Setter Function
     */

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDepartment() {
        return department;
    }
}
