import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Student extends HustPerson {
    private SimpleDateFormat check ;
    Database database;
    /**
     *
     * Constructor
     *
     * @param studentName is name of student
     * @param studentId is id number of student
     * @param birthday is student birthday
     * @param major is student major
     */
    public Student(String studentName,String studentId,String birthday,String major){
        database = Main.database;
        this.name = studentName;
        this.id = studentId;
        this.major = major;
        this.setBirthday(birthday);
    }

    /**
     *
     * Overloading constructor
     *
     * add parameter "department"
     * @param department
     *
     */

    public Student(String studentName,String studentId,String birthday,String major, String department){
        this(studentName,studentId,birthday,major);
        this.department = department;
    }

    //Get_Set Function

    public void setBirthday(String birthday){
        this.check =  new SimpleDateFormat("dd/MM/yyyy");
        this.check.setLenient(false);
        try {
            this.birthday = check.parse(birthday);
        } catch (ParseException e) {
            System.out.println("invalid date");
        }
    }

    public void printInformation(){
        System.out.println("_____________________________________________________________");
        System.out.println(" ");
        System.out.println("        Name         : " + this.getName());
        System.out.println("        Id           : " + this.getId());
        System.out.println("        Birthday     : " + this.getBirthday());
        System.out.println("        Major        : " + this.getMajor());
        System.out.println("        Department   : " + this.getDepartment());

        System.out.println("_____________________________________________________________");

    }

}
