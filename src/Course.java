/*
 * Created by Le Cong Thanh in 5/10/2018
 *
 */


public class Course extends HustObject {
    private Subject subject;
    private Lecturer lecturer;

    /**
     *
     * @param id
     * @param subject
     * @param lecturer
     */
    public Course(String id, Subject subject, Lecturer lecturer){
        this.id = id;
        this.name = subject.getName();
        this.department = subject.getDepartment();
        this.subject = subject;
        this.lecturer = lecturer;
        lecturer.addSubject(subject);
    }



    public Subject getSubject() {
        return subject;
    }

    public Lecturer getLecturer() {
        return lecturer;
    }

    public void printInformation(){
        System.out.println("_____________________________________________________________");
        System.out.println(" ");
        System.out.println("        Name         : " + this.getName());
        System.out.println("        Id           : " + this.getId());
        System.out.println("        Department   : " + this.getDepartment());
        System.out.println("        Giang vien   : " + this.getLecturer().getName());
        System.out.println("_____________________________________________________________");

    }

}
