import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    static Database database = new Database();
    public static void main(String [] agrs) throws IOException {
        System.out.println("Hello");
        boolean done = false;
        int nChoose;
        while (!done){
            System.out.println("Chon chuc nang : ");
            System.out.println("1. Quan ly dao tao ");
            System.out.println("2. Quan ly sinh vien ");
            System.out.println("3. Tra cuu ");
            nChoose = scanner.nextInt();
            switch (nChoose){
                case 1:
                    System.out.println("Chon chuc nang :");
                    System.out.println("1. Them sinh vien ");
                    System.out.println("2. Them giao vien ");
                    System.out.println("3. Mo hoc phan ");
                    System.out.println("4. Mo lop hoc ");
                    quanLyDaoTao();
                    break;
                case 2:
                    System.out.println("1.Dang ky hoc");
                    System.out.println("2.Nhap diem");
                    quanLySinhVien();
                    break;
                case 3:
                    System.out.println("Chon doi tuong :");
                    System.out.println("1.Sinh vien");
                    System.out.println("2.Giang vien");
                    System.out.println("3.Mon hoc");
                    System.out.println("4.Khoa hoc");
                    traCuu();
                    break;
            }
        }

    }
    public static void quanLyDaoTao(){
        int nChoose = scanner.nextInt();
        switch (nChoose){
            case 1:
                themSinhvien();
                break;
            case 2:
                themGiangvien();
                break;
            case 3:
                moHocPhan();
                break;
            case 4:
                moLophoc();
                break;
        }
    }
    public static void themSinhvien(){
        String[] info = new String[5];
        System.out.println("Nhap cac thong tin sau :");
        scanner.nextLine();
        System.out.println("Ten sinh vien          :");
        info[0] = scanner.nextLine();
        System.out.println("Ma so sinh vien        :");
        info[1] = scanner.nextLine();
        System.out.println("Ngay sinh              :");
        info[2] = scanner.nextLine();
        System.out.println("Chuyen nganh           :");
        info[3] = scanner.nextLine();
        System.out.println("Don vi                 :");
        info[4] = scanner.nextLine();
        Student student = new Student(info[0],info[1],info[2],info[3],info[4]);
        database.add(student);
        student.printInformation();
    }
    public static void themGiangvien(){
        String[] info = new String[5];
        System.out.println("Nhap cac thong tin sau :");
        scanner.nextLine();
        System.out.println("Ten giang vien         :");
        info[0] = scanner.nextLine();
        System.out.println("Ma giang vien          :");
        info[1] = scanner.nextLine();
        database.add(new Lecturer(info[0],info[1]));
    }
    public static void moHocPhan(){
        String[] info = new String[5];
        int credit;
        System.out.println("Nhap cac thong tin sau :");
        scanner.nextLine();
        System.out.println("Ten hoc phan           :");
        info[0] = scanner.nextLine();
        System.out.println("Ma hoc phan            :");
        info[1] = scanner.nextLine();
        System.out.println("Don vi                 :");
        info[2] = scanner.nextLine();
        System.out.println("So tin chi             :");
        credit = scanner.nextInt();
        database.add(new Subject(info[1],info[0],info[2],credit));
    }
    public static void moLophoc(){
        String[] info = new String[5];
        System.out.println("Nhap cac thong tin sau :");
        scanner.nextLine();
        System.out.println("Ma lop hoc             :");
        info[1] = scanner.nextLine();
        System.out.println("Ma hoc phan            :");
        info[2] = scanner.nextLine();
        System.out.println("Ma giang vien          :");
        info[3] = scanner.nextLine();
        database.add(new Course(info[1],database.getSubjects(info[2]),database.getLecturer(info[3])));
    }

    public static void quanLySinhVien(){
        int nChoose = scanner.nextInt();
        switch (nChoose){
            case 1:
                Dangkyhoc();
                break;
            case 2:
                Nhapdiem();
                break;
        }
    }

    private static void Nhapdiem() {
        String studentId ;
        String subjectId ;
        double point;
        scanner.nextLine();
        System.out.println("Nhap ma sinh vien :");
        studentId = scanner.nextLine();
        System.out.println("Nhap ma lop hoc   :");
        subjectId = scanner.nextLine();
        System.out.println("Nhap diem : ");
        point = scanner.nextDouble();
        PointList pointList = database.getPointList();
        pointList.setPoint(studentId,subjectId,point);
    }

    private static void Dangkyhoc() {
        String id ;
        scanner.nextLine();
        System.out.println("Nhap ma sinh vien :");
        id = scanner.nextLine();
        Student student = database.getStudents(id);
        System.out.println("Nhap ma lop hoc   :");
        id = scanner.nextLine();
        Course course = database.getCourses(id);
        Point point = new Point(course.getSubject().getId(),course.getSubject().getName(),
                course.getSubject().getCredit(),student.getId(),student.getName());
        database.getPointList().add(point);
    }


    public static void traCuu(){
        int nChoose = scanner.nextInt();
        switch (nChoose){
            case 1:
                Sinhvien();
                break;
            case 2:
                Giangvien();
                break;
            case 3:
                HocPhan();
                break;
            case 4:
                Lophoc();
                break;
        }
    }

    private static void Lophoc() {
        String id ;
        scanner.nextLine();
        System.out.println("Nhap id :");
        id = scanner.nextLine();
        Course course = database.getCourses(id);
        course.printInformation();
        List<Point> list = database.getPointList().getStudentList(course.getSubject().getId());
        for (int i = 0; i < list.size(); i++) {
            list.get(i).printInfomation();
        }
    }

    private static void HocPhan() {
        String id ;
        scanner.nextLine();
        System.out.println("Nhap id :");
        id = scanner.nextLine();
        database.getSubjects(id).printInformation();
    }

    private static void Giangvien() {
        String id ;
        scanner.nextLine();
        System.out.println("Nhap id :");
        id = scanner.nextLine();
        database.getLecturer(id).printInformation();
    }

    private static void Sinhvien() {
        String id ;
        scanner.nextLine();
        System.out.println("Nhap id :");
        id = scanner.nextLine();
        database.getStudents(id).printInformation();
        Student student = database.getStudents(id);
        List<Point> list = database.getPointList().getPointList(student.getId());
        for (int i = 0; i < list.size(); i++) {
            list.get(i).printInfomation();
        }
    }


}

