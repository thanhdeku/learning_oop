import java.util.ArrayList;
import java.util.List;

public class PointList {
    List<Point> pointList;
    public PointList(){
        pointList = new ArrayList<>();
    }
    public void add(Point point){
        if(pointList.size() == 0){
            pointList.add(point);
        }else {
            for (int i = 0; i < pointList.size(); i++) {
                if(pointList.get(i).getStudentId().equals(point.getStudentId())
                        && pointList.get(i).getSubjectId().equals(point.getSubjectId())){
                    System.out.println("existing");

                }
                else {
                    pointList.add(point);
                }
            }
        }
    }
    public void setPoint(String studentId, String subjectId,double point){
        Point pointex = this.getPoint(studentId,subjectId);
        if(pointex == null){
            System.out.println("invalid id");
        }else {
            pointex.setPoint(point);
        }
    }
    public Point getPoint(String studentId, String subjectId){
        for (Point aPointList : pointList) {
            if (aPointList.getStudentId().equals(studentId)
                    && aPointList.getSubjectId().equals(subjectId)) {
                return aPointList;
            }
        }
        return null;
    }

    public List<Point> getPointList(String id){
        List<Point> list = new ArrayList<>();
        for (int i = 0; i < pointList.size(); i++) {
            if(pointList.get(i).getStudentId().equals(id)){
                list.add(pointList.get(i));
            }
        }
        return list;
    }
    public List<Point> getStudentList(String id){
        List<Point> list = new ArrayList<>();
        for (int i = 0; i < pointList.size(); i++) {
            if(pointList.get(i).getSubjectId().equals(id)){
                list.add(pointList.get(i));
            }
        }
        return list;
    }


    public List<Point> getPointList() {
        return pointList;
    }
}

