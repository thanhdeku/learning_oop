/*
 * Created by Le Cong Thanh in 5/10/2018
 *
 */
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Lecturer extends HustPerson {
    List<Subject> subjectList;
    private SimpleDateFormat check ;
    public Lecturer(String studentName, String id){
        this.name = studentName;
        this.id = id;
        subjectList = new ArrayList<>();
    }
    public Lecturer(String studentName, String id, String major, String department){
        this(studentName,id);
        this.major = major;
        this.department = department;
        subjectList = new ArrayList<>();
    }
    public Lecturer(String studentName, String id, String birthday, String major, String department){
        this(studentName,id,major,department);
        this.setBirthday(birthday);
    }
    public void setBirthday(String birthday){
        this.check =  new SimpleDateFormat("dd/MM/yyyy");
        this.check.setLenient(false);
        try {
            this.birthday = check.parse(birthday);
        } catch (ParseException e) {
            System.out.println("invalid date");
        }
    }

    public void addSubject(Subject subject){
        subjectList.add(subject);
    }

    public void printInformation(){
        System.out.println("_____________________________________________________________");
        System.out.println(" ");
        System.out.println("        Name         : " + this.getName());
        System.out.println("        Id           : " + this.getId());
        System.out.println("        Birthday     : " + this.getBirthday());
        System.out.println("        Major        : " + this.getMajor());
        System.out.println("        Department   : " + this.getDepartment());
        System.out.println("_____________________________________________________________");

    }


}
